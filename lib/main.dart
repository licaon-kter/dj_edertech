import 'package:flutter/material.dart';
import 'package:dj_edertech/provider/player_provider.dart';
import 'package:dj_edertech/provider/theme_provider.dart';
import 'package:dj_edertech/screens/about_screen.dart';
import 'package:dj_edertech/screens/main_screen.dart';
import 'package:provider/provider.dart';

void main() {
  runApp(
    MultiProvider(
      providers: [
        ChangeNotifierProvider(create: (context) => PlayerProvider()),
        ChangeNotifierProvider(create: (context) => ThemeProvider()),
      ],
      child: const MaterialApp(home: App()),
    ),
  );
}

class App extends StatelessWidget with WidgetsBindingObserver {
  const App({super.key});

  static const titleApp = 'DJ Edertech';

  @override
  Widget build(BuildContext context) {
    return Consumer<ThemeProvider>(
      builder: (context, themeProvider, child) {
        return MaterialApp(
          title: titleApp,
          theme: themeProvider.getTheme(),
          initialRoute: '/',
          routes: {
            '/': (context) => const MainScreen(title: titleApp),
            '/about': (context) => const AboutScreen()
          },
        );
      },
    );
  }
}
