import 'package:flutter/material.dart';

ElevatedButton buttonWidget(IconData icon, VoidCallback onPressed) {
  return ElevatedButton(
      style: ButtonStyle(
        minimumSize: MaterialStateProperty.all(const Size(40, 40)),
        padding: MaterialStateProperty.all(const EdgeInsets.all(10)),
        textStyle: MaterialStateProperty.all(const TextStyle(fontSize: 16)),
      ),
      onPressed: onPressed,
      child: Icon(icon));
}
