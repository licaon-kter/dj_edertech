import 'package:flutter/material.dart';
import 'package:dj_edertech/theme/theme_switcher.dart';
import 'package:dj_edertech/widgets/player_buttons.dart';
import 'package:dj_edertech/widgets/playlist.dart';
import 'package:dj_edertech/widgets/progress_bar.dart';

class MainScreen extends StatefulWidget {
  const MainScreen({super.key, required this.title});

  final String title;

  @override
  State<MainScreen> createState() => _MainScreenState();

}

class _MainScreenState extends State<MainScreen> with AutomaticKeepAliveClientMixin {
  @override
  bool get wantKeepAlive => true;

  @override
  Widget build(BuildContext context) {
    super.build(context);
    final themeSwitcher = ThemeSwitcher.of(context);

    return Scaffold(
      appBar: AppBar(
        backgroundColor: Theme.of(context).appBarTheme.backgroundColor,
        title: Text(widget.title),
        bottom: const PreferredSize(
            preferredSize: Size.fromHeight(24.0),
            child: Column(children: [
              Text(
                'DJ Edertech Sets',
                style: TextStyle(fontSize: 16.0, fontWeight: FontWeight.bold),
              ),
              SizedBox(height: 10.0),
            ])),
        actions: [
          IconButton(
            icon: const Icon(Icons.info),
            onPressed: () {
              Navigator.pushNamed(context, '/about');
            },
          ),
          IconButton(
            icon: const Icon(Icons.lightbulb),
            onPressed: () {
              themeSwitcher.switchTheme();
            },
          ),
        ],
      ),
      body: _buildBody(),
    );
  }

  Center _buildBody() {
    return const Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Expanded(child: PlayList()),
          ProgressBar(),
          PlayerButtons(),
          SizedBox(height: 20.0),
        ],
      ),
    );
  }
}
